var express = require('express');
var session = require('express-session');
var http = require('http');
var path = require('path');
var favicon = require('static-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var busboy = require('connect-busboy');
var compression = require('compression');
var hbs = require('hbs');

var systemRoutes = require('./lib/core/server/routes');
exports.Model = require('./lib/core/server/models/model');
exports.Neo4jModel = require('./lib/core/server/models/model-neo4j');

exports.init = function(config) {
    var server = express();
    server.use(busboy());
    server.locals.config = config;

    server.use(function(req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        return next();
    });

    server.set('view engine', 'hbs');
    server.set('views', config.views);
    server.use(express.static(config.static));
    hbs.registerPartials(config.views + '/partials');
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded());
    server.use(cookieParser());
    server.use(favicon());
    server.use(session({
        secret: 'BlkL4b',
        saveUninitialized: true,
        resave: true
    }));
    server.use(compression({
        threshold: 512
    }))

    //routes
    //server.use('/', systemRoutes.site);
    for (var r in config.routes) {
        server.use(config.routes[r].path, config.routes[r].module);
    }
    server.use(function(req, res, next) {
        res.status(404);

        // respond with html page
        if (req.accepts('html')) {
            res.render('common/404', {
                url: req.url
            });
            return;
        }

        // respond with json
        if (req.accepts('json')) {
            res.send({
                error: 'Not found'
            });
            return;
        }

        // default to plain-text. send()
        res.type('txt').send('Not found');
    });

    return server;
};
