var neo4j = require('neo4j-js');

var Model = function(options) {
    var self = this;
    this.super_ = {};
    this._schema = {};
    this.collection_name = options.collection || '';
    this.data = {};
    //this.graph = new neo4j.GraphDatabase('http://localhost:7474');
    neo4j.connect('http://10.0.0.11:7474/db/data/', function(err, graph) {
        if (err)
            console.log(err);
        self.graph = graph;
    });
    this.collection = this.collection_name;
}

Model.get_host = function(req) {
    return req.headers.referer.replace('http://', '').replace('www', '').split('/')[0];
}

Model.prototype.schema = function(obj) {
    var self = this;
    var keys = Object.keys(obj);
    for (i = 0; i < keys.length; i++) {
        var key = keys[i];
        if (self._schema.hasOwnProperty(obj[key]) === false) {
            self._schema[key] = obj[key];
            var def = obj[key].default;
            if (def) {
                self.data[key] = self[key] = def;
            } else {
                self.data[key] = self[key] = null;
            }
        }
    }
    return self;
}

Model.prototype.get = function(key) {
    if (key in this.data) {
        return this.data[key];
    } else {
        return null;
    }
}

Model.prototype.set = function(key, val) {
    if (key in this.data) {
        this.data[key] = val;
    }
    return this;
}

Model.prototype.load = function(query, data, callback) {
    var self = this;

    this.graph.query(query, data, function(err, results) {
        if (err) {
            console.log(err);
            console.log(err.stack);
        }
        callback(err, results);
    });
}

Model.prototype.findOne = function(query, data, callback) {
    var self = this;
    this.graph.query(query, data, function(err, results) {
        if (err) {
            console.log(err);
            console.log(err.stack);
        }
        callback(err, results);
    });
}

Model.prototype.insert = function(query, data, callback) {
    //MATCH (website:Website {domain:"dev"}) CREATE (page:Page {title:"Home"}) CREATE (website)-[:LINKSTO]->(page)
    this.graph.query(query, data, function(err, results) {
        if (err) {
            console.log(err);
            console.log(err.stack);
        }
		callback(err, JSON.stringify(results, null, 5));
    });
}

Model.prototype.update = function(query, data, callback) {
    var self = this;
    this.graph.query(query, data, function(err, results) {
        if (err) {
            console.log(err);
            console.log(err.stack);
        }
        callback(err, JSON.stringify(results, null, 5));
    });
}

Model.prototype.del = function(query, data, callback) {
    var self = this;
    this.graph.query(query, data, function(err, results) {
        if (err) {
            console.log(err);
            console.log(err.stack);
        }
		callback(err, JSON.stringify(results, null, 5));
    });
}

Model.extend = function(methods, options) {
    var self = function(options) {
        Model.call(this, options);
    }

    self.prototype = Object.create(Model.prototype);
    self.prototype.constructor = self;

    for (method in methods) {
        if (self.prototype.hasOwnProperty(method) === false) {
            self.prototype[method] = methods[method];
        }
    }
    return self;
}

module.exports = Model;
