var mongo = require('mongoskin');

var Model = function(options){
	this.super_ = {};
	this._schema = {};
	this.collection_name = options.collection || '';
	this.data = {};
	this.db_name = options.db || 'dev';
	this.db = mongo.db("mongodb://localhost:27017/" + this.db_name, {safe:true});
	this.collection = this.db.collection(this.collection_name);

	console.log("Using " + this.db_name);
}

Model.prototype.schema = function(obj){
	var self = this;
	var keys = Object.keys(obj);
	for(i=0; i<keys.length; i++){
		var key = keys[i];
		if(self._schema.hasOwnProperty(obj[key]) === false){
			self._schema[key] = obj[key];
			var def = obj[key].default;
			if(def){
				self.data[key] = self[key] = def;
			}else{
				self.data[key] = self[key] = null;
			}
		}
	}
	return self;
}

Model.prototype.get = function(key){
	if(key in this.data){
		return this.data[key];
	}else{
		return null;
	}
}

Model.prototype.set = function(key, val){
	if(key in this.data){
		this.data[key] = val;
	}
	return this;
}

Model.prototype.load = function(query, callback){
	var self = this;
	this.collection.find(query).toArray(function(err, items){
		if(err) return next(err)
		items = items || [];
		callback(err, items);
	});
}

Model.prototype.findOne = function(query, callback){
	var self = this;
	this.collection.findOne(query, function(err, item){
		if(err) return next(err)
		item = item || [];
		callback(err, item);
	});
}

Model.prototype.update = function(query, qupdate, callback){
	var self = this;
	this.collection.update(query, {$set:qupdate}, {w:1}, function(err, result){
		if(err) return next(err)
		callback(err, result);
	});
}

Model.prototype.del = function(query, callback){
	var self = this;
	this.collection.remove(query, {w:1}, function(err, result){
		if(err) return next(err)
		callback(err, result);
	});
}

Model.extend = function(methods, options){
	var self =  function(options){
		Model.call(this, options);
	}

	self.prototype = Object.create(Model.prototype);
	self.prototype.constructor = self;

	for(method in methods){
		if(self.prototype.hasOwnProperty(method) === false) {
			self.prototype[method] = methods[method];
		}
	}
	return self;
}

module.exports = Model;
