server {
	listen 80;
	server_name [[servername]];
	root /var/www/[[servername]];
	index index.html index.htm;

	location /index.html {
	}

	location /public/assets/images/ {
		alias    /var/www/[[servername]]/public/assets/images/;
		expires max;
		access_log off;
	}

	location /public/js/ {
		alias /var/www/[[servername]]/public/js/;
		access_log off;
	}

	location /js/ {
		alias /var/www/[[servername]]/public/js/;
		access_log off;
	}

	location /public/css/ {
		alias /var/www/[[servername]]/public/css/;
		access_log off;
	}

	location /css/ {
		alias /var/www/[[servername]]/public/css/;
		access_log off;
	}

	if (!-d $request_filename) {
		rewrite ^/(.+)/$ /$1 permanent;
	}

	location / {
		proxy_pass http://127.0.0.1:[[port]]/;
	}
}
